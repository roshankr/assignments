class A:
    def __init__(self,name):
        self.name  = name
    def do_this(self):
        print 'class A'

class B:
    def __init__(self,name):
        self.name  = name
    def do_this(self):
        print 'class B'
class C(B,A):
    pass

c = C('reshu')
c.do_this()

